
<div class="pageContent">


<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: #e6344d !important;
    border-radius: 20px;
    padding: 20px !important;
    color: white !important;
    border:2px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #ccc;
  }
  .ourvalues img{
    height:70px;
  }

  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }
  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

  }
</style>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 no-padding">
 <!--  <h1>L'entraide<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    <img class="img-responsive" src='<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/custom/csc/banniere_logo.jpg'> 
  </div>
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
    <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#fff; max-width:100%; float:left;">
      <div class="col-xs-12 no-padding" style="text-align:center;margin-top:100px;"> 
        <div class="col-xs-12 no-padding">
          <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-100px;margin-bottom:5px;background-color: #fff;font-size: 14px;">
              <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
                        <h3 class="col-xs-12 text-center">
              <i class="fa fa-th"></i> <?php echo Yii::t("home", "Un outil pour s'entraider") ?><br>
              <small>
                <b>Centres sociaux connectés :</b> <?php echo Yii::t("home", "L'innovation au service de la collaboration") ?><br>
                <?php //echo Yii::t("home", "created for citizens actors of change") ?>
              </small>
              <hr style="width:40%; margin:20px auto; border: 4px solid #cecece;">
            </h3>
            <div class="col-xs-12">
              <a href="javascript:;" data-hash="#annonces?section=offer" class=" btn-main-menu lbh-menu-app col-xs-12 col-sm-4 col-md-4 col-md-offset-2 col-sm-offset-2 padding-20 margin-top-5 margin-right-5" data-type="classifieds" >
                  <div class="text-center">
                      <div class="col-md-12 no-padding text-center">
                          <h4 class="no-margin text-white">
                            <i class="fa fa-search"></i>
                            <?php echo Yii::t("home","Les offres") ?>
                            <!--  <br><small class="text-white">
                                  <?php echo Yii::t("home","Retrouver toutes les annonces à disposition")?>
                              </small>-->
                          </h4>
                      </div>
                  </div>
              </a>
              <a href="javascript:;" data-hash="#annonces?section=need" class="btn-main-menu lbh-menu-app col-xs-12 col-sm-4 col-md-4 padding-20 margin-top-5" data-type="search" >    
                  <div class="text-center">
                      <!-- <h4 class="text-red no-margin "><i class="fa fa-search"></i>
                          <span class="homestead"> <?php //echo Yii::t("home","SEARCH") ?></span>
                      </h4><br/> -->
                      <div class="col-md-12 no-padding text-center">
                          <h4 class="no-margin text-white">
                            <i class="fa fa-bullhorn"></i>
                            <?php echo Yii::t("home","Les besoins") ?>
                              <!--<br>
                              <small class="text-white">
                                  <?php echo Yii::t("home","Aider, soutenir, venir en aide") ?>
                              </small>-->
                          </h4>
                      </div>
                  </div>
              </a>
            </div>
            <div class="col-xs-12 no-padding margin-top-20">
                        <!-- <div class="col-md-1 col-sm-1 hidden-xs"></div> -->
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15" style="text-align:center;">
                <a href="https://larbrisseau.com/" target="_blank">
                  <img class="img-responsive" style="margin:0 auto;" 
                   src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/custom/csc/logo-arbrisseau.jpg"/>
                   Centre social et culturel de l'Arbrisseau
                </a>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15">
                <a href="http://www.centre-social-lazare-garreau-lille.fr/" target="_blank"><img class="img-responsive" style="margin:0 auto;" 
                   src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/custom/csc/Logo-header-CSCLG.png"/>
                   Centre social et culturel Lazare Garreau</a>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15" style="text-align:center;">
                <a href="https://www.cheminrouge.fr/" target="_blank">
                  <img class="img-responsive" style="margin:0 auto;" 
                   src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/custom/csc/logo-mcr.png"/>
                   Centre social intercommunal La Maison du Chemin Rouge
                </a>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15">
                <a href="http://www.association-projet.org/" target="_blank">
                <img class="img-responsive" style="margin:0 auto;" 
                   src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/custom/csc/logo-projet.png"/>
                   Centre social Projet
                </a>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15">
                <a href="http://www.lsi-asso.fr/" target="_blank">
                  <img class="img-responsive" style="margin:0 auto;" 
                   src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/custom/csc/lsi.png"/>
                   Lille Sud Insertion 
                </a>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15">
                <a href="#@laFabriqueDuSud" class="lbh" target="_blank">
                  <img class="img-responsive" style="margin:0 auto;" 
                     src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/custom/csc/logo-fabrique-du-sud.png"/>
                     La Fabrique du Sud
                </a>
              </div> 
            </div>
        </div>
      </div>
    </div>

       
      
    </div>
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 contact-map" style="color:#293A46;float:left;" id="contactSection">
    <center>
      <h4 style="text-transform: inherit;line-height: 19px;"><i class="fa fa-at headerIcon"></i> <span style="vertical-align: bottom;line-height: 30px;">contact@csconnectes.eu</span></h4>
      <h4 class="" style="text-transform: inherit;line-height: 19px;"><i class="fa fa-globe headerIcon"></i>  <a href="http://csconnectes.eu" target="_blank" style="vertical-align: bottom;line-height: 30px;">www.csconnectes.eu</a></h4>
     
    <center>
  </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 no-padding">
 <!--  <h1>L'entraide<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    <img class="img-responsive" src='<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/custom/csc/logo-feder.jpg'> 
  </div>

<script type="text/javascript">

<?php $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
    $this->renderPartial($layoutPath.'home.peopleTalk'); ?>
var peopleTalkCt = 0;

jQuery(document).ready(function() {
  setTimeout(function(){
    //$("#videoDocsImg img").css({"max-height":$("#form-home-subscribe").outerHeight()});
  },300);
  topMenuActivated = false;
  hideScrollTop = true;
  checkScroll();
  loadLiveNow();
  $(".videoSignal").click(function(){
    openVideo();
  });

  peopleTalkCt = getRandomInt(0,peopleTalk.length);
  showPeopleTalk();


    $("#map-loading-data").hide();
  $(".mainmenu").html($("#modalMainMenu .links-main-menu").html());
  //$("#modalMainMenu .links-main-menu").html("");

  //setTimeout(function(){ $("#input-communexion").hide(300); }, 300);

  var timerCo = false;
      
  $("#main-search-bar").keyup(function(){
    if($("#main-search-bar").val().length > 2){
      if(timerCo != false) clearTimeout(timerCo);
      timerCo = setTimeout(function(){ 
        //$("#info_co").html("");
        $(".info_co").addClass("hidden");
        $("#change_co").addClass("hidden");
        searchType = ["cities"];
        loadingData=false;
        scrollEnd=false;
        totalData = 0;
        communexion.state = false ; 
        startSearch(0, 20);
      }, 500);
    }else{
      $(".info_co").removeClass("hidden");
      $("#dropdown_search").html("");
    }
  });


    $("#change_co").click(function(){
      $(".info_co, .input_co").removeClass("hidden");
    $("#change_co").addClass("hidden");

    });


  setTitle("<?php echo Yii::t("home","Welcome on") ?> <span class='text-red'>commune</span>cter","home","<?php echo Yii::t("home","Welcome on Communecter") ?>");
  $('.tooltips').tooltip();

  $("#btn-param-postal-code").click(function(){
    $("#div-param-postal-code").show(400);
  });

  $(".btn-show-map-home").click(function(){
    search.app="search";
    initCountType();
      initTypeSearch("all");
      $(this).html("<i class='fa fa-spin fa-circle-o-notch'></i> "+trad.currentlyloading);
    startSearch(0, 30, function(){
      if(typeof formInMap != "undefined" && formInMap.actived == true)
        formInMap.cancel(true);
        //else if(isMapEnd == false && notEmpty(contextData) && location.hash.indexOf("#page.type."+contextData.type+"."+contextData.id))
      //  getContextDataLinks();
      else{
        if(isMapEnd == false && contextData && contextData.map && location.hash.indexOf("#page.type."+contextData.type+"."+contextData.id) )
          Sig.showMapElements(Sig.map, contextData.map.data, contextData.map.icon, contextData.map.title);
          showMap();
      }
      $(".btn-show-map-home").html("<i class='fa fa-map-marker'></i> "+trad.showmap);
    });
  })
  // $('#searchBarPostalCode').keyup(function(e){
 //        clearTimeout(timeoutSearchHome);
 //        timeoutSearchHome = setTimeout(function(){ startSearch(); }, 800);
 //    });


    $(".explainLink").click(function() {
    showDefinition( $(this).data("id") );
    return false;
  });
    $(".keyword").click(function() {
      $(".keysUsages").hide();
      link = "<br/><a href='javascript:;' class='showUsage homestead yellow'><i class='fa fa-toggle-up' style='color:#fff'></i> Usages</a>";
      $(".keywordExplain").html( $("."+$(this).data("id")).html()+link ).fadeIn(400);
       $(".showUsage").off().on("click",function() { $(".keywordExplain").slideUp(); $(".keysUsages").slideDown();});
    });

    $(".keyword1").click(function() {
      $(".keysKeyWords").hide();
      link = "<br/><a href='javascript:;' class='showKeywords homestead yellow'><i class='fa fa-toggle-up' style='color:#fff'></i> Mots Clefs</a>";
      $(".usageExplain").html( $("."+$(this).data("id")).html()+link ).slideDown();
       $(".showKeywords").off().on("click",function() { $(".usageExplain").slideUp(); $(".keysKeyWords").slideDown();});
    });


    $(".btn-main-menu").mouseenter(function(){ 
        $(".menuSection2").addClass("hidden"); 
        if( $(this).data("type") ) 
            $("."+$(this).data("type")+"Section2").removeClass("hidden");
    }).click(function(e) {  
        e.preventDefault(); 
        $('#modalMainMenu').modal("hide"); 
        mylog.warn("***************************************"); 
        mylog.warn("bindLBHLinks",$(this).attr("href")); 
        mylog.warn("***************************************"); 
        var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href"); 
        urlCtrl.loadByHash( h ); 
    }); 

    $(".tagSearchBtn").click(function(e) {  
        e.preventDefault(); 
        $('#modalMainMenu').modal("hide"); 
        mylog.warn( ".tagSearchBtn",$(this).data("type"),$(this).data("stype"),$(this).data("tags") ); 

        searchObj.types = $(this).data("type").split(",");
        
        if( $(this).data("stype") )
            searchObj.stype = $(this).data("stype");
        else
            searchObj.tags = $(this).data("tags");
        
        urlCtrl.loadByHash($(this).data("app"));
        urlCtrl.afterLoad = function () {     
            //we have to pass by a variable to set the values         
            searchType = searchObj.types;
        
            if( $(this).data("stype") )
                $('#searchSType').val(searchObj.stype);
            else
                $('#searchTags').val(searchObj.tags);
            startSearch();
            searchObj = {};
        }
    }); 

});
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function showPeopleTalk(step)
{
  // if(!step)
  //  step = 1;
  // peopleTalkCt = peopleTalkCt+step;
  // if( undefined == peopleTalk[ peopleTalkCt ]  )
  //  peopleTalkCt = 0;
  // person = peopleTalk[ peopleTalkCt ];

  var html = "";
  $.each(peopleTalk, function(key, person){
  html += '<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 padding-5" style="min-height:430px;max-height:430px;">' +
        '<div class="" style="max-height:240px; overflow:hidden; max-width:100%;">' +
        '<img class="img-responsive img-thumbnail peopleTalkImg" src="'+person.image+'"><br>' +
        '</div>' +
        '<span class="peopleTalkName">'+person.name+'</span><br>' +
        '<span class="peopleTalkProject">'+person.project+'</span><br>' +
        '<span class="peopleTalkComment inline-block">'+person.comment+'</span>' +
      '</div>';
  });

  $("#co-friends").html( html );
  // $(".peopleTalkName").html( person.name );
  // $(".peopleTalkImg").attr("src",person.image);
  // $(".peopleTalkComment").html("<i class='fa fa-quote-left'></i> "+person.comment+"<i class='fa fa-quote-right'></i> ");
  // $(".peopleTalkProject").html( "<a target='_blank' href='"+person.url+"'>"+person.project+"</a>" );

}

function openVideo(){
  $("#videoDocsImg").fadeOut("slow",function() {
    heightCont=$("#form-home-subscribe").outerHeight();
    $(".videoWrapper").height(heightCont);
    $(".videoWrapper").fadeIn('slow');
     var symbol = $("#autoPlayVideo")[0].src.indexOf("?") > -1 ? "&" : "?";
      //modify source to autoplay and start video
      $("#autoPlayVideo")[0].src += symbol + "autoplay=1";
      if($("#form-home-subscribe").length)
        $(".videoWrapper .h_iframe").css({"margin-top": ((heightCont-$(".videoWrapper .h_iframe").height())/2)+"px"});
  });
}

var timeoutSearchHome = null;

function showTagOnMap (tag) {

  mylog.log("showTagOnMap",tag);

  var data = {   "name" : tag,
           "locality" : "",
           "searchType" : [ "persons" ],
           //"searchBy" : "INSEE",
                 "indexMin" : 0,
                 "indexMax" : 500
                };

        //setTitle("","");$(".moduleLabel").html("<i class='fa fa-spin fa-circle-o-notch'></i> Les acteurs locaux : <span class='text-red'>" + cityNameCommunexion + ", " + cpCommunexion + "</span>");

    $.blockUI({
      message : "<h1 class='homestead text-red'><i class='fa fa-spin fa-circle-o-notch'></i> Recherches des collaborateurs ...</h1>"
    });

    showMap(true);

    $.ajax({
        type: "POST",
            url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
            data: data,
            dataType: "json",
            error: function (data){
               mylog.log("error"); mylog.dir(data);
            },
            success: function(data){
              if(!data){ toastr.error(data.content); }
              else{
                mylog.dir(data);
                Sig.showMapElements(Sig.map, data);
                $.unblockUI();
              }
            }
    });

  //loadByHash('#project.detail.id.56c1a474f6ca47a8378b45ef',null,true);
  //Sig.showFilterOnMap(tag);
}



function loadLiveNow () {
  mylog.log("loadLiveNow CO2.php");
  var searchParams = {
    "tpl":"/pod/nowList",
    "searchLocalityCITYKEY" : new Array(""),
    "indexMin" : 0, 
    "indexMax" : 30 
  };

    //console.log("communexion : ", communexion);
  if($("#searchLocalityCITYKEY").val() != ""){
    searchParams.searchLocalityCITYKEY = new Array($("#searchLocalityCITYKEY").val());
  }else if(myScopes.communexion.values != null){
    if(typeof myScopes.communexion.values.cityKey != "undefined"){
      searchParams.searchLocalityCITYKEY = new Array(myScopes.communexion.values.cityKey);
    }
  }

  var searchParams = {
    "tpl":"/pod/nowList",
    "searchLocality" : getSearchLocalityObject(true),
    "indexMin" : 0, 
    "indexMax" : 30 
  };
    
    //console.log("communexion ?", communexion);

    ajaxPost( "#nowList", baseUrl+'/'+moduleId+'/element/getdatadetail/type/0/id/0/dataName/liveNow?tpl=nowList',
          searchParams, function(data) {
          bindLBHLinks();
  } , "html" );
}


</script>





