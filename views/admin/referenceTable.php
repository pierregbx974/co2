<?php
//echo CHtml::scriptFile(Yii::app()->request->baseUrl. '/plugins/DataTables/media/js/jquery.dataTables.min.1.10.4.js');
//echo CHtml::cssFile(Yii::app()->request->baseUrl. '/plugins/DataTables/media/css/DT_bootstrap.css');
//echo CHtml::scriptFile(Yii::app()->request->baseUrl. '/plugins/DataTables/media/js/DT_bootstrap.js');
$cssAnsScriptFilesModule = array(
    '/plugins/jquery-simplePagination/jquery.simplePagination.js',
	'/plugins/jquery-simplePagination/simplePagination.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getRequest()->getBaseUrl(true));

$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
//header + menu
/*$this->renderPartial($layoutPath.'header', 
                    array(  "layoutPath"=>$layoutPath , 
                            "page" => "admin") );*/ 
?>
<style type="text/css">
.simple-pagination li a, .simple-pagination li span {
    border: none;
    box-shadow: none !important;
    background: none !important;
    color: #2C3E50 !important;
    font-size: 16px !important;
    font-weight: 500;
}
.simple-pagination li.active span{
	color: #d9534f !important;
    font-size: 24px !important;	
}
</style>
<div class="panel panel-white col-xs-3 no-padding">
	<ul class="col-xs-12 list-group">
		<li class="col-xs-12 list-group-item">
			<a href="javascript:;" class="sourceFilter" style="cursor:pointer;">
				<i class="fa fa-at fa-2x"></i>
				<?php echo Yii::t("admin", "source"); ?>
				<span id="count-source" class="badge"><?php echo (@$countMenu && @$countMenu["source"]) ? $countMenu["source"]: 0 ; ?></span>
			</a>
		</li>
		<li class="col-xs-12 list-group-item">
			<a href="javascript:;" class="referenceFilter" style="cursor:pointer;">
				<i class="fa fa-link fa-2x"></i>
				<?php echo Yii::t("admin", "Reference"); ?>
				<span id="count-reference" class="badge"><?php echo (@$countMenu && @$countMenu["reference"]) ? $countMenu["reference"]: 0 ; ?></span>
			</a>
		</li>
		<li class="col-xs-12 list-group-item">
			<a href="javascript:;" class="searchOpenFilter" style="cursor:pointer;">
				<i class="fa fa-search fa-2x"></i>
				<?php echo Yii::t("admin", "search & reference"); ?>
			</a>
		</li>
	</ul>
</div>
<div class="panel panel-white col-xs-9 no-padding">
	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<div id="" class="" style="width:80%;  display: -webkit-inline-box;">
	                <input type="text" class="form-control" id="input-search-table" 
	                        placeholder="search by name or by #tag, ex: 'commun' or '#commun'">
	    <button class="btn btn-default hidden-xs menu-btn-start-search-admin btn-directory-type">
	        <i class="fa fa-search"></i>
	    </button>
	    </div>
    </div>
	<div class="panel-heading border-light">
		<h4 class="panel-title"><i class="fa fa-globe fa-2x text-green"></i> Filtered by types : </h4>
		<?php foreach ($typeDirectory as $value) { ?>
			<a href="javascript:;" onclick="applyStateFilter('<?php echo $value ?>')" class="filter<?php echo $value ?> btn btn-xs btn-default active btncountsearch"> <?php echo $value ?> <span class="badge badge-warning countPeople" id="count<?php echo $value ?>"> <?php echo @$results["count"][$value] ?></span></a>
		<?php } ?>
	</div>
	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20 text-center"></div>
	<div class="panel-body">
		<div>	
			<table class="table table-striped table-bordered table-hover  directoryTable" id="panelAdmin">
				<thead>
					<tr>
						<th>Type</th>
						<th>Name</th>
						<th>Tags</th>
						<th>Scope</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody class="directoryLines">
					<?php 
					$memberId = Yii::app()->session["userId"];
					$memberType = Person::COLLECTION;
					$tags = array();
					$scopes = array(
						"codeInsee"=>array(),
						"codePostal"=>array(),
						"region"=>array(),
					);
					
					?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>
</div>
<script type="text/javascript">
var openingFilter = "<?php echo ( isset($_GET['type']) ) ? $_GET['type'] : '' ?>";
var directoryTable = null;
var contextMap = {
	"tags" : <?php echo json_encode($tags) ?>,
	"scopes" : <?php echo json_encode($scopes) ?>,
};
var results = <?php echo json_encode($results) ?>;
var initType = (typeof paramsAdmin != "undefined" 
			&& typeof paramsAdmin["reference"] != "undefined"
			&& typeof paramsAdmin["reference"]["initType"] != "undefined") ? paramsAdmin["reference"]["initType"]: ["organizations", "events", "projects"];
var sourceKey = (typeof custom != "undefined" && custom.slug) ? custom.slug : null;
var icons = {
	organizations : "fa-group",
	projects : "fa-lightbulb-o",
	events : "fa-calendar",
	citoyens : "fa-user",
	services : "fa-sun-o",
	classifieds : "fa-bullhorn",
	poi : "fa-map-marker",
	news : "fa-newspaper-o"
};
var searchAdmin={
	text:null,
	page:"",
	type:initType[0],
	//open : true,
	sourceKey:sourceKey,
	mode:"source"
};
jQuery(document).ready(function() {
	setTitle("Espace administrateur : Répertoire","cog");
	initKInterface();
	initViewTable(results);
	initMenuReference();
	if(openingFilter != "")
		$('.filter'+openingFilter).trigger("click");
	$("#input-search-table").keyup(function(e){
        if(e.keyCode == 13){
            searchAdmin.page=0;
            searchAdmin.text = $(this).val();
            if(searchAdmin.text=="")
            	searchAdmin.text=true;
            startAdminSearch(true);
            // Init of search for count
            if(searchAdmin.text===true)
            	searchAdmin.text=null;
         }
    });
    initPageTable(results.count.citoyens);

});	
function initMenuReference(){
	$(".sourceFilter").off().on("click", function(){
		searchAdmin.mode="source";
		searchAdmin.page=0;
		startAdminSearch(true);
	});
	$(".referenceFilter").off().on("click", function(){
		searchAdmin.mode="reference";
		searchAdmin.page=0;
		startAdminSearch(true);
	});

	$(".searchOpenFilter").off().on("click", function(){
		searchAdmin.mode="open";
		searchAdmin.page=0;
		startAdminSearch(true);
	});
}
function initPageTable(number){
	numberPage=(number/100);
	$('.pageTable').pagination({
        items: numberPage,
        itemOnPage: 15,
        currentPage: 1,
        hrefTextPrefix:"?page=",
        cssStyle: 'light-theme',
        //prevText: '<span aria-hidden="true">&laquo;</span>',
        //nextText: '<span aria-hidden="true">&raquo;</span>',
        onInit: function () {
            // fire first page loading
        },
        onPageClick: function (page, evt) {
            // some code
            //alert(page);
            searchAdmin.page=(page-1);
            startAdminSearch();
        }
    });
}
function initViewTable(data){
	$('#panelAdmin .directoryLines').html("");
	//showLoader('#panelAdmin .directoryLines');
	console.log("valuesInit",data);
	$.each(data,function(type,list){
		$.each(list, function(key, values){
			entry=buildDirectoryLine( values, type, type, icons[type]);
			$("#panelAdmin .directoryLines").append(entry);
		});
	});
	bindAdminBtnEvents();
	//resetDirectoryTable() ;
}
function refreshCountBadgeAdmin(count){
	$.each(count, function(e,v){
		$("#count"+e).text(v);
	});
}
function startAdminSearch(initPage){

    //$("#second-search-bar").val(search);
    $('#panelAdmin .directoryLines').html("Recherche en cours. Merci de patienter quelques instants...");
    $.ajax({ 
        type: "POST",
        url: baseUrl+"/"+moduleId+"/admin/reference/tpl/json",
        data: searchAdmin,
        dataType: "json",
        success:function(data) { 
	          initViewTable(data.results);
	          bindAdminBtnEvents();
	          if(typeof data.results.count !="undefined")
	          	refreshCountBadgeAdmin(data.results.count);
	          console.log(data.results);
	          if(initPage)
	          	initPageTable(data.results.count[searchAdmin.type]);
        },
        error:function(xhr, status, error){
            $("#searchResults").html("erreur");
        },
        statusCode:{
                404: function(){
                    $("#searchResults").html("not found");
            }
        }
    });
}

function buildDirectoryLine( e, collection, type, icon/* tags, scopes*/ ){
		strHTML="";
		if(typeof e._id =="undefined" || ((typeof e.name == "undefined" || e.name == "") && (e.text == "undefined" || e.text == "")) )
			return strHTML;
		actions = "";
		classes = "";
		id = e._id.$id;
		var status=[];
		/* **************************************
		* ADMIN STUFF
		***************************************** */
		

		/* **************************************
		* TYPE + ICON
		***************************************** */
	strHTML += '<tr id="'+type+id+'">'+
		'<td class="'+collection+'Line '+classes+'">'+
			'<a href="#page.type.'+type+'.id.'+id+'" class="lbh" target="_blank">';
				if (e && typeof e.profilThumbImageUrl != "undefined" && e.profilThumbImageUrl!="")
					strHTML += '<img width="50" height="50" alt="image" class="img-circle" src="'+baseUrl+e.profilThumbImageUrl+'">'+e.type;
				else 
					strHTML += '<i class="fa '+icon+' fa-2x"></i> '+type;
			strHTML += '</a>';
		strHTML += '</td>';
		
		/* **************************************
		* NAME
		***************************************** */
		if(typeof e.name != "undefined")
			title=e.name;
		else if(typeof e.text != "undefined")
			title=e.text;
		strHTML += '<td><a href="#page.type.'+type+'.id.'+id+'" class="lbh" target="_blank">'+title+'</a></td>';
		
		/* **************************************
		* TAGS
		***************************************** */
		strHTML += '<td>';
		if(typeof e.tags != "undefined"){
			$.each(e.tags, function(key,value){
				strHTML += ' <a href="javascript:;" onclick="applyTagFilter(\''+value+'\')"><span class="label label-inverse text-red">'+value+'</span></a>';
				//if( tags != "" && !in_array($value, tags) ) 
				//	array_push($tags, $value);
			});
		}
		strHTML += '</td>';

		/* **************************************
		* SCOPES
		***************************************** */
		strHTML += '<td>';
		/*if( typeof e.address != "undefined" && isset( $e["address"]['codeInsee']) ){
			$strHTML .= ' <a href="#" onclick="applyScopeFilter('.$e["address"]['codeInsee'].')"><span class="label label-inverse">'.$e["address"]['codeInsee'].'</span></a>';
			if( !in_array($e["address"]['codeInsee'], $scopes['codeInsee']) ) 
				array_push($scopes['codeInsee'], $e["address"]['codeInsee'] );
		}*/
		if( typeof e.address != "undefined"){
			if(typeof e.address.streetAddress != "undefined" ){
				strHTML += ' <a href="javascript:;" onclick="applyScopeFilter('+e.address.streetAddress+')" class="letter-blue"><span class="">'+e.address.streetAddress+'</span></a><br/>';
			//if( !in_array($e["address"]['codePostal'], $scopes['codePostal']) ) 
			//	array_push($scopes['codePostal'], $e["address"]['codePostal'] );
			}
			if(typeof e.address.postalCode != "undefined" ){
				strHTML += ' <a href="javascript:;" onclick="applyScopeFilter('+e.address.postalCode+')" class="letter-blue"><span class="">'+e.address.postalCode+'</span></a>';
			//if( !in_array($e["address"]['codePostal'], $scopes['codePostal']) ) 
			//	array_push($scopes['codePostal'], $e["address"]['codePostal'] );
			}
			if(typeof e.address.addressLocality != "undefined"){
				strHTML += ' <a href="javascript:;" onclick="applyScopeFilter('+e.address.addressLocality+')" class="letter-blue"><span class="">'+e.address.addressLocality+'</span></a><br/>';
			}
			if(typeof e.address.level1Name){
				strHTML += '<a href="javascript:;" onclick="applyScopeFilter('+e.address.level1Name+')" class="letter-blue"><span class="">'+e.address.level1Name+'</span></a><br/>';
			//if( !in_array($e["address"]['region'], $scopes['region']) ) 
			//	array_push($scopes['region'], $e["address"]['region'] );
			}
			if(typeof e.address.addressCountry){
				strHTML += '<a href="javascript:;" onclick="applyScopeFilter('+e.address.addressCountry+')" class="letter-blue"><span class="">'+e.address.addressCountry+'</span></a><br/>';
			//if( !in_array($e["address"]['region'], $scopes['region']) ) 
			//	array_push($scopes['region'], $e["address"]['region'] );
			}	
		}
		strHTML += '</td>';
		strHTML += '<td class="center status">';
			console.log(status);
			if(notEmpty(status)){
				$.each(status,function(e,v){
					strHTML+="<span class='badge bg-primary "+v.key+"'>"+v.label+"</span>";
				});
			}else{
				strHTML += "No status";
			}
		strHTML += '</td>';
		/* **************************************
		* ACTIONS
		***************************************** */
		if(searchAdmin.mode=="source"){
			action='<button data-id="'+id+'" data-type="'+type+'" data-action="remove" data-setkey="source" class="margin-right-5 setSourceAdmin btn bg-red text-white"><i class="fa fa-ban"></i> Remove from source</button>';
		}else if (searchAdmin.mode=="reference"){
			action='<button data-id="'+id+'" data-type="'+type+'" data-action="remove" data-setkey="reference" class="margin-right-5 setSourceAdmin btn bg-red text-white"><i class="fa fa-ban"></i> Remove from reference</button> ';
		}
		else if (searchAdmin.mode=="open"){
			action='<button data-id="'+id+'" data-type="'+type+'" data-action="add" data-setkey="reference" class="margin-right-5 setSourceAdmin btn bg-green text-white"><i class="fa fa-plus"></i> Add as reference</button>';
		}
		strHTML += '<td class="center">'; 
			strHTML += '<div class="btn-group">'+action
						'</div>';
		strHTML += '</td>';
	
	strHTML += '</tr>';
	return strHTML;
}

function resetDirectoryTable() 
{ 
	/*mylog.log("resetDirectoryTable");

	if( !$('.directoryTable').hasClass("dataTable") )
	{
		directoryTable = $('.directoryTable').dataTable({
			"aoColumnDefs" : [{
				"aTargets" : [0]
			}],
			"oLanguage" : {
				"sLengthMenu" : "Show _MENU_ Rows",
				"sSearch" : "",
				"oPaginate" : {
					"sPrevious" : "",
					"sNext" : ""
				}
			},
			"aaSorting" : [[1, 'asc']],
			"aLengthMenu" : [[5, 10, 15, 20, -1], [5, 10, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"iDisplayLength" : 10,
		});
	} 
	else 
	{
		if( $(".directoryLines").children('tr').length > 0 )
		{
			directoryTable.dataTable().fnDestroy();
			directoryTable.dataTable().fnDraw();
		} else {
			mylog.log(" directoryTable fnClearTable");
			directoryTable.dataTable().fnClearTable();
		}
	}*/
}

function applyStateFilter(str)
{
	//mylog.log("applyStateFilter",str);
	searchAdmin.type=str;
	searchAdmin.page=0;
	$(".btncountsearch").removeClass("active");
	$(".filter"+str).addClass("active");
	startAdminSearch(true);
	//directoryTable.DataTable().column( 0 ).search( str , true , false ).draw();
}
/*function clearAllFilters(str){ 
	directoryTable.DataTable().column( 0 ).search( str , true , false ).draw();
	directoryTable.DataTable().column( 2 ).search( str , true , false ).draw();
	directoryTable.DataTable().column( 3 ).search( str , true , false ).draw();
}*/
function applyTagFilter(str)
{
	mylog.log("applyTagFilter",str);
	if(!str){
		str = "";
		sep = "";
		$.each($(".btn-tag.active"), function() { 
			mylog.log("applyTagFilter",$(this).data("id"));
			str += sep+$(this).data("id");
			sep = "|";
		});
	} else 
		clearAllFilters("");
	mylog.log("applyTagFilter",str);
	directoryTable.DataTable().column( 2 ).search( str , true , false ).draw();
	return $('.directoryLines tr').length;
}

function applyScopeFilter(str)
{
	//mylog.log("applyScopeFilter",$(".btn-context-scope.active").length);
	if(!str){
		str = "";
		sep = "";
		$.each( $(".btn-context-scope.active"), function() { 
			mylog.log("applyScopeFilter",$(this).data("val"));
			str += sep+$(this).data("val");
			sep = "|";
		});
	} else 
		clearAllFilters("");
	mylog.log("applyScopeFilter",str);
	directoryTable.DataTable().column( 3 ).search( str , true , false ).draw();
	return $('.directoryLines tr').length;
}

function bindAdminBtnEvents(){
	mylog.log("bindReferenceBtnEvents");
	$(".setSourceAdmin").off().on("click", function(){
		var btnClick=$(this);
		if(btnClick.data("setkey")=="source" && btnClick.data("action")=="remove"){
			bootbox.confirm("BE carefull, remove a source from an element is not revokable !!<br/>Are you sure to continue ?", function(result) {
				if (result) {
					setSourceAdmin(btnClick);
				}
			});
		}else
			setSourceAdmin(btnClick);
	});
		

}
function setSourceAdmin(btnClick){
	var action=btnClick.data("action");
	var setKey=btnClick.data("setkey");
	var params={
		id:btnClick.data("id"),
		type:btnClick.data("type")
	};
	if(typeof custom != "undefined" && notNull(custom) && custom.slug){
		params.origin="custom";
		params.sourceKey=custom.slug;
	} 
	$.ajax({
        type: "POST",
        url: baseUrl+"/"+moduleId+"/admin/setsource/action/"+action+"/set/"+setKey,
        data : params
    })
    .done(function (data)
    {
        if ( data && data.result ) {
        	toastr.success(data.msg);
        	$("#"+params.type+params.id).fadeOut();
        	countB=parseInt($("#count-"+setKey).text());
        	if(action=="remove")
        		countB--;
        	else
        		countB++;
        	$("#count-"+setKey).text(countB);
        	//window.location.href = baseUrl+"/"+moduleId;
        } else {
           toastr.error("something went wrong!! please try again.");
        }
    });
}

function changeButtonName(button, action) {
	mylog.log(action);
	var icon = '<span class="fa-stack"> <i class="fa fa-user fa-stack-1x"></i><i class="fa fa-check fa-stack-2x stack-right-bottom text-danger"></i></span>';
	if (action=="addBetaTester") {
		button.removeClass("addBetaTesterBtn");
		button.addClass("revokeBetaTesterBtn");
		button.html(icon+" Revoke this beta tester");
	} else if (action=="revokeBetaTester") {
		button.removeClass("revokeBetaTesterBtn");
		button.addClass("addBetaTesterBtn");
		button.html(icon+" Add this beta tester");
	} else if (action=="addSuperAdmin") {
		button.removeClass("addSuperAdminBtn");
		button.addClass("revokeSuperAdminBtn");
		button.parents().eq(4).find(".status").append("<span class='badge bg-primary superAdmin'>Super Admin</span>");
		button.html('<span class="fa-stack"> <i class="fa fa-user-plus fa-stack-1x"></i><i class="fa fa-times fa-stack-2x stack-right-bottom text-danger"></i></span>'+" Revoke this super admin");
	} else if (action=="revokeSuperAdmin") {
		button.removeClass("revokeSuperAdminBtn");
		button.addClass("addSuperAdminBtn");
		button.parents().eq(4).find(".status .superAdmin").remove();
		button.html(icon+" Add this super admin");
	} else if (action=="addBannedUser") {
		button.removeClass("banUserBtn");
		button.addClass("unbanUserBtn");
		button.parents().eq(4).find(".status").append("<span class='badge bg-primary isBanned'>Is banned</span>");
		button.html(icon+" Unban user");
	}else if (action=="revokeBannedUser") {
		button.removeClass("unbanUserBtn");
		button.addClass("banUserBtn");
		button.parents().eq(4).find(".status .isBanned").remove();
		button.html('<span class="fa-stack"> <i class="fa fa-user fa-stack-1x"></i><i class="fa fa-ban fa-stack-2x stack-right-bottom text-danger"></i></span>'+" Ban user");
	} else {
		mylog.warn("Unknown action !");
	}
}

</script>